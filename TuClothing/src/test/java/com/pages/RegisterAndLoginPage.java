package com.pages;

import org.junit.Assert;
import org.openqa.selenium.By;

import com.runner.BaseClass;

public class RegisterAndLoginPage extends BaseClass {
	public void verifytheregisterbutton( ) {
		driver.findElement(By.cssSelector("[href='/login']")).click();
		driver.findElement(By.cssSelector(".ln-c-button.ln-c-button--primary.regToggle")).click();
	}
	public void verifyenterregisterpage() {
	
		Assert.assertEquals("https://tuclothing.sainsburys.co.uk/login",driver.getCurrentUrl()); 
	}
	public void verifyenterregistrationform() {
		driver.findElement(By.cssSelector("#register_email")).click();
		driver.findElement(By.cssSelector("#register_email")).sendKeys("Sam@gmail.com");
		driver.findElement(By.cssSelector("#register_title")).click();		
		driver.findElement(By.cssSelector("#register_title")).sendKeys("MR");
		driver.findElement(By.cssSelector("#register_firstName")).click();
		driver.findElement(By.cssSelector("#register_firstName")).sendKeys("Sam");
		driver.findElement(By.cssSelector("[name=lastName]")).click();
		driver.findElement(By.cssSelector("[name=lastName]")).sendKeys("Tv");
		driver.findElement(By.cssSelector("#password")).click();
		driver.findElement(By.cssSelector("#password")).sendKeys("abc1234");
		driver.findElement(By.cssSelector("#register_checkPwd")).click();
		driver.findElement(By.cssSelector("#register_checkPwd")).sendKeys("abc1234");
		driver.findElement(By.cssSelector("#submit-register")).click();		
	}	
	public void verifyregistercompletesuccessfull() {
	 Assert.assertEquals("https://tuclothing.sainsburys.co.uk/register/success",driver.getCurrentUrl()); 

	}
	}
				


