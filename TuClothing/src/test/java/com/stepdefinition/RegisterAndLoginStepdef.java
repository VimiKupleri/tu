package com.stepdefinition;

import com.runner.BaseClass;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class RegisterAndLoginStepdef extends BaseClass {

@Given("^user is in register page$")
public void user_is_in_register_page() throws Throwable {
	registerandloginpage.verifyenterregisterpage(); 
}

	
	@When("^user fill the registration form$")
	public void user_fill_the_registration_form() throws Throwable {
		registerandloginpage.verifyenterregistrationform();
		
	}
	@Then("^it should show registration complete successfully$")
	public void it_should_show_registration_complete_successfully() throws Throwable {
		registerandloginpage.verifyregistercompletesuccessfull();
		
	}		
	}
